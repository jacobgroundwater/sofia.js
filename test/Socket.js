var fs     = require("fs");
var vm     = require("vm");
var util   = require("util");
var assert = require('assert');

var data = fs.readFileSync("../src/install/Socket.js");

var package;
var boxed = "proxy("+data+");";
var proxy = function(e){ package=e; }
vm.runInContext(boxed, vm.createContext( {proxy:proxy} ));

package.require = require;
package.binding = function(){return function(){console.log(arguments);} };
package["server.listen"]({ 
	port  : 8977,
	iface : "localhost",
	handle : function(){console.log(arguments);}
},function(err,dat){
	assert.ifError(err);
	package["server.close"]();
});
