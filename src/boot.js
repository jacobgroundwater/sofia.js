var timers = require("timers");
var util   = require("util");
var cli    = require("cli-color");
var fs     = require("fs");
var vm     = require("vm");
var util   = require("util");
var events = require("events");
var crypto = require("crypto");

var sofia = require("./system.js");

var argv  = process.argv;
var init      = "db.json"; //argv[2],
	verbose   = false; //argv[3],
	adminPort = 9898; //argv[4],
	startups  = false;// argv[5];

var ini = {};

ini.db = JSON.parse( fs.readFileSync(init) );
ini.processes = {};
ini.emitter = new events.EventEmitter;
ini.network = {};
ini.stashes = {};

var kern = new sofia.Kernel( ini );

function bootSandbox(code,reply){

		var package;
		var proxy   = function(e){package=e;}
		var context = vm.createContext({proxy:proxy});
		
		try {
			vm.runInContext("proxy("+code+");",context);
			reply(null,package);
		} catch (Exception) {
			reply(Exception,null);
		}

}

var packageCache = {};
var sandboxPackage = {
	"init" : function(args,reply){
		reply(null,"OK");
	},
	"class.load" : function(packagePath,reply){
		var This=this;
		var vm = this.require("vm");

		this.queue("object.read",packagePath,function(err,object){
			if (err) return reply(err,null);
			
			var package = {};
			var hash = crypto.createHash('sha1');
			
			for (key in object){
				hash.update( key.toString() );
				hash.update( object.toString() );
			}
			
			var digest = hash.digest('base64');
			if (!packageCache[digest]) {
				try {
				
					var method;
					var proxy   = function(e){method=e;}
					var context = vm.createContext({proxy:proxy});
				
					for (name in object){
						if (name.substring(0,1)=="_") continue;
						var code = object[name];
						vm.runInContext(
							"proxy("+code+");",
							context,
							packagePath+".sbx-"+name+".js");
						package[name]=method;
					}
					package.__path__ = packagePath;
					packageCache[digest] = package;
					reply(null,package);

				} catch (Exception) {

					This.error("VM Execution Failed");

					reply(Exception,null);
				}
			} else {
				reply(null,packageCache[digest]);
			}
		});
	}
}

var filesysProcess = fs.readFileSync("types/CouchDb.js");
var initialProcess = fs.readFileSync("types/Init.js");

bootSandbox(filesysProcess,function(err,filesysPackage){
	if (err) throw err;
	kern.init(filesysPackage,{},function(err,filesysPid){
		if (err) throw err;
		kern.fileSystemId = filesysPid;
		kern.proc(filesysPid,function(err,filesysProc){
			if (err) throw err;
			filesysProc.bind("object.read",function(err,objectReadPath){
				if (err) throw err;
				filesysProc.bind("object.save",function(err,objectSavePath){
					if (err) throw err;
					var sandboxBind = {
						"object.read" : objectReadPath,
						"object.save" : objectSavePath,
					}
					kern.init(sandboxPackage,sandboxBind,function(err,sandboxPid){
						if (err) throw err;
						kern.proc(sandboxPid,function(err,sandboxProc){
							if (err) throw err;
							sandboxProc.bind("class.load",function(err,classLoadPath){
								var initialBind = {
									"class.load"  : classLoadPath,
								}

								kern.proc(filesysPid,function(err,filesysProc){
									if (err) throw err;
									filesysProc.push("init",{},function(err,_){
										if (err) throw err;

										// Manually Start the Main Init Process
										bootSandbox(initialProcess,function(err,initialPackage){ 
											if (err) throw err;
											kern.init(initialPackage,initialBind,function(err,initialPid){ 
												if (err) throw err;
												kern.proc(initialPid,function(err,initialProc){ 
													if (err) throw err;

													// Inject the kernel
													initialProc.kernel=kern; 

													var initServices = {services:{
														"#FILESYS/object.save":objectSavePath,
														"#FILESYS/object.read":objectReadPath,
														"#SANDBOX/class.load":classLoadPath
													}};

													f = function(list,callback){
														var x= list.pop();
														if(!x) callback();

													}

													initialProc.push("init",initServices,function(err,_){ 
														if (err) throw err;
														filesysProc.push("object.read","/init",function(err,object){
															if (err) throw err;
															var initItems = object.init;
															var emitter = new events.EventEmitter;
															emitter.on('next',function(i){
																var path = initItems[i];
																if (path) {
																	filesysProc.push("object.read",path,function(err,item){
																		if (err) console.error(path,err);

																		initialProc.push("process.start",item,function(err,pid){
																			if (err) throw err;
																			emitter.emit('next',i+1);
																		})

																	})
																}
															})
															emitter.emit('next',0);
														})
													})
												})
											}) 
										})
									})
								})	
							})						
						})
					})
				})
			})
		})
	})
})








