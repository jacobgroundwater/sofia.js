#!/usr/bin/env node

var child   = require('child_process');
var opti    = require('optimist');

var argv = opti
	.usage('Usage: $0')
	.options('database', {
		alias    : 'd',
		default  : 'db.json',
		describe : 'Read boot database from FILE',
	})
	.options('bootfile', {
		alias    : 'b',
		default  : null,
		describe : 'Boot each node process from FILE'
	})
	.options('adminport',{
		alias    : 'p',
		default  : 8000,
		describe : 'Admin ports binds to PORT'
	})
	.options('verbose',{
		alias    : 'v',
		default  : 'info',
		describe : 'Set logging level'
	})
	.options('help',{
		alias    : 'h',
		default  : false,
		describe : 'Display help on ITEM'
	})
	.options('install',{
		alias    : 'I',
		default  : null,
		describe : 'Run installer from FILE'
	})
	.options('manifest',{
		alias    : 'm',
		default  : 'manifest.json',
		describe : 'Use FILE as installation manifest'
	})
	.options('startup',{
		alias    : 's',
		default  : 'startup.json',
		describe : 'Initialize with Startup FILE'
	})
	.argv;

if (argv.help) {
	opti.showHelp();
	process.exit(0);
}

// Do Install
if (argv.install) {
	var cp = child.spawn('node',[
		argv.install,
		argv.database,
		argv.manifest,
		argv.verbose,
	],{customFds: [0, 1, 2],});
	if (argv.bootfile) {
		cp.on('exit',start);
	}
} else if (argv.bootfile) {
	start(0);
} else {
	opti.showHelp();
}

function start(code){
	if (code===0) {
		var cp = child.spawn('node',[
			argv.bootfile,
			argv.database,
			argv.verbose,
			argv.adminport,
			argv.startup,
		],{customFds: [0, 1, 2],});
	} else {
		console.error("Install Process Failed");
	}
}








