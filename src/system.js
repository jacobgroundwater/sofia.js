var cli     = require('cli-color');
var events  = require('events');
var fs      = require('fs');
var path    = require('path');
var sprintf = require('sprintf');
var timers  = require('timers');
var util    = require('util');
var vm      = require('vm');
var fmt     = require('sprintf').sprintf;
var crypto  = require('crypto');

var UUID = function(reply){
	var hash = crypto.createHash('sha1');
	fs.open("/dev/urandom",'r',function(err,fd){
		var buffer = new Buffer(128);
		fs.read(fd,buffer,0,128,null,function(err,bytes){
			hash.update(buffer);
			reply(null,hash.digest('base64'));
		});
	});
}

var fmt = sprintf.sprintf;

var Panic = function(){
	if (arguments.length==0) return new Error();
	arguments[0] = cli.bgRed(arguments[0]);
	console.error.apply( null,arguments );
	return new Error( util.format.apply(null,arguments) );
}

var $ = function(object) {
	var inspect = util.inspect(object);
	var suffix  = ( inspect.length < 80 ) ? "" : "...";
	return inspect.substring(0,80).replace(/\s+/g," ") + suffix;
}

var Process = function ( emitter, pid ){
	
	this.require = require;
	this.pid = pid; // Deprecate
	this.Panic = Panic;
	this.$ = util.inspect;
	this.uuid = UUID;
	this.tick = process.nextTick;
	
	this.proc = function(pid,reply){
		emitter.emit('proc',pid,reply);
	}
	
	this.bind = function(port,reply){
		emitter.emit('bind',port,reply);
	}
	
	this.info    = function(){
		arguments[0] = cli.bgGreen.black(arguments[0]);
		console.log.apply(null,arguments);
	}
	this.error   = function(message){
		arguments[0] = cli.bgRed(arguments[0]);
		console.log.apply(null,arguments);
	}
	this.debug   = function(message){
		arguments[0] = cli.cyan(arguments[0]);
		console.log.apply(null,arguments);
	}
	this.warning = function(message){
		arguments[0] = cli.bgYellow(arguments[0]);
		console.log.apply(null,arguments);
	}
	
	// TODO: Make this queue work for *ALL* queues, local and network
	this.queue = function(port,message,callback){
		emitter.emit('message-queued',port,message,callback);
	}
	
	this.push = function( port, message, callback ) {
		emitter.emit('message-pushed',port,message,callback);
	}

}

var Stash = function(process,emitter){
	this.process=process;
	this.emitter=emitter;
}

var Kernel = function( ini ){
	var processes = ini.processes;
	var emitter   = ini.emitter;
	var network   = ini.network;
	var stashes   = ini.stashes;
	var db        = ini.db
		
	// Get a process by id
	this.proc = function(id,reply){
		if (!processes[id]) return reply(Panic("Process %s Not Found",id),null);
		return "Kernel.proc: " + reply(null,processes[id])
	}
	
	// List all active processes
	this.list = function(reply){
		var keys = [];
		for (key in processes){
			keys.push(key);
		}
		return "Kernel.list: " + reply(null,keys);
	}
	
	// 
	this.kill = function(id,reply){
		return "Kernel.kill: " + reply(Panic("Not Yet Supported"),null);
	}
	
	// Init and run a process from a package
	this.init = function ( package, bind, reply ){
		UUID( function(err,uuid ){	
			
			var emitter = new events.EventEmitter;
			var proc    = new Process(emitter,uuid);
		
			processes[uuid] = proc;
		
			stashes  [uuid] = new Stash(proc,emitter);
			
			emitter.on('message-queued',function(port,message,callback){
				if (port.indexOf("/")!=0) {
					if (!bind[port]) {
						return "message-queued:" + callback(
							Panic("Local Port %s Missing",port),null);
					}
					path = bind[port];
				} else {
					path = port;
				}
				if (!network[path]) {
					return "message-queued:" + callback(
						Panic("Network Port %j Missing",path),null);
				}
				network[path][0].push(network[path][1],message,callback);
			});
		
			emitter.on('message-pushed',function(port,message,callback){
				if (!package[port]) {
					console.error("Package: ", util.inspect(package,true));
					return "message-pushed:" + callback(
						Panic("Port %s Not Found in Package %s",port,$(package)),null);
				}
				package[port].apply( proc, [message,callback] );
			});
			
			emitter.on('bind',function(port,reply){
				UUID(function(err,hash){
					var path    = "/" + hash + "/" + port;

					network[path] = [proc,port];

					return "bind:" + reply(null,path);
				});
			});
		
			emitter.on('proc',function(pid,reply){
				if (!processes[pid]) {
					return reply(Panic("No Process Found for %s",pid),null);
				}
				return "proc:" + reply(null,processes[pid]);
			});
		
			return "init:" + reply(null,uuid);
		});
	}
}

exports.Kernel = Kernel;
exports.$ = $;





















