{
	
	"init" : function(jail,reply){
		this.jail = jail;
		return "Multiplexer[init]" + reply(null,"OK");
	},
	"object.read" : function( path, reply ){
		var Path = this.require("path");
		var jailPath = Path.join( this.jail, path );
		
		return "Multiplexer[object.read]" + this.queue("object.read",jailPath,reply);
	},
	"object.save" : function( message, reply ){
		var Path = this.require("path");
		
		if (!message)       return reply(new Error("Missing Message"),null);
		if (!message.value) return reply(new Error("Value Missing"),  null);
		if (!message.path && message.path!=="")  return reply(new Error("Path Missing"),   null);
		if (typeof message.value != 'object') return reply(new Error("Malformed Value"),null);
		
		var path  = message.path;
		var value = message.value;
		
		var jailPath = Path.join( this.jail, path );
		
		return "Multiplexer[object.save]" + this.queue("object.save",{
			path  : jailPath,
			value : value
		},reply);
	}	
}