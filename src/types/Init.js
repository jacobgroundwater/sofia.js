{
	"init" : function(ini,reply){
		var This = this;
		if (!ini) ini={};
		
		this.services = ini.services ? ini.services : {};
		
		provides = ["init.fork","init.find","system.stat","process.start"];
		
		var f = function( list, callback ){
			var x = list.pop();
			if(!x) return callback(null,"OK");
			
			return This.bind(x,function(err,path){
				if (err) return "Init[init]:" + callback(err,null);
				var name = "#INIT/" + x;
				This.services[name] = path;
				return This.tick( function(){
					f(list,callback);
				});
			})
		}
		return "Init[init]" + f(provides,reply);
	},
	"init.fork" : function(args,reply){
		var This = this;
		var name = args.name;
		var init = args.init;
		var item = {
			name    : name,
			depends : {"class.load":"#SANDBOX"},
			package : "/Class/Init",
			init    : init
		};
		
		return "Init[init.fork]" + This.push("process.start",item,function(err,pid){
			if (err) { 
				This.warning("Proccess.start error %j",err);
				return reply(this.Panic("Failed to Fork Init"),null); 
			}
			
			return This.kernel.proc(pid,function(err,proc){
				
				if (err){
					This.error("Init.fork failed getting PID %s: %j",pid,err);
					return "Init[init.fork]:" + reply(This.Panic("Fork Failed"),null);
				}
				
				proc.kernel = This.kernel;
				
				return "Init[init.fork]:" +reply(null,pid);
			})
			
		});
	},
	"init.find" : function(name,reply){
		var path = this.services[name];
		if (!path) {
			This.warning("Path Not Specified in Init.find");
			return reply(This.Panic("Service Not Found"),null);
		}
		return "Init[init.find]" + reply(null,path);
	},
	"process.bind" : function(args,reply){ // TODO: Deprecate
		var kernel = this.kernel;
		
		var proc = args.proc;
		var port = args.port;
		
		return "Init[process.bind]" + kernel.bind( proc, port, reply );
	},
	"process.start" : function( startupItem ,reply){
		var kern = this.kernel;
		var This = this;
		
		var util = this.require("util");
		
		var name = startupItem.name;
		var deps = startupItem.depends;
		var path = startupItem.package;
		var init = startupItem.init;
		
		var provide = startupItem.provide;
		
		var bind = {};
		for(port in deps){
			var serviceName = deps[port] + "/" + port;
			bind[port] = this.services[serviceName];
			if (!bind[port]) {
				This.error("Service [%s] Missing Binding for Service [%s]",
					name, serviceName);
				return "Init[process.start]" + 
					reply(This.Panic("Bound Port Missing"),null);
			}
		}
		This.debug("Starting New Process %s",name);
		return "Init[process.start]" + This.queue("class.load",path,function(err,package){
			if (err) return "Init[process.start]" + reply(This.Panic(),null);
			
			return kern.init(package,bind,function(err,pid){
				if (err) return "Init[process.start]" + reply(This.Panic(),null);
				
				return kern.proc(pid,function(err,proc){
					if (err) return "Init[process.start]" + reply(This.Panic(),null);
					
					var f = function ( list, callback ) {
						if (!list) return "Init[process.start]" + callback();
						
						var portType = list.pop();
						if (!portType) return "Init[process.start]" + callback();
						
						return proc.bind( portType, function(err,networkPath){
							var serviceName = name + "/" + portType;
							This.services[serviceName] = networkPath;
							return This.tick(function(){
								f(list,callback)
							})
						})
						
					};
					
					return f( provide, function(){
						if (init){
							return proc.push( "init", init, function(err,dat){
								if (err) return "Init[process.start]" + reply(This.Panic("%j",err),null);
								return "Init[process.start]" + reply(null,pid);
							});
						} else {
							return "Init[process.start]" + reply(null,pid);
						}
						
					})
				})
			})
		})
	},
	"system.stat" : function(opts,reply){
		var out;
		switch(opts) {
			case "list":
				out = this.services;
				break;
			default :
				out = "Sofia.js - Version 0.0.2a";
		}
		return "Init[system.stat]" + reply(null,out);
	}
}