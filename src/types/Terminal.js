{
	"init" : function(regex,reply){
		var This=this;
		return "Terminal[init]" + This.bind("terminal.repl",function(err,path){
			if (err) return reply(err,null);
			return This.queue("service.register",{regex:regex,path:path},reply);
		})
	},
	"terminal.repl" : function(message,reply){
		var This = this;
		var response = {message:"Error"};
		
		This.debug("%j",message);
		
		switch(message.path){
			case "/start":
				This.debug("Received Data %j",message.data);
				return this.queue("process.start",message.data,function(err,pid){
					return reply(null,{content:pid});
				});
			case "/message":
				return this.queue("init.find",{},function(err,rep){
					//
				});
			case "/list" : 
				return this.queue("system.stat","list",function(err,rep){
					return reply(null,{mimetype:"application/json",content:rep});
				});
			default:
				return reply(null,{content:"EMPTY"});
		}
	}
}