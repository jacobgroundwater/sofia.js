{
	"object.put" : function(object,reply){
		var This = this;
		return "ContentDb[object.put]" + This.uuid(function(err,uid){
			return This.queue("object.save",{
				path  : uid,
				value : object
			},function(err,_){
				if (err) return reply(err,null);
				return reply(null,uid);
			});
		});
	},
	"object.get" : function(uid,reply){
		return "ContentDb[object.get]" + this.queue("object.read",uid,reply);
	},
	"branch.get" : function(name,reply){
		if (typeof name != 'string') {
			return reply(new Error("Branch Hash Invalid"),null);
		}
		return "ContentDb[branch.get]" + this.queue("object.read",name,function(err,object){
			if (err) return reply(err,null);
			return reply(null,object.key);
		});
	},
	"branch.set" : function(args,reply){
		if (typeof args.hash != 'string') {
			return reply(new Error("Branch Hash Invalid"),null);
		}
		return "ContentDb[branch.set]" + this.queue("object.save",{
			path  : args.name,
			value : {key:args.hash}
		},reply);
	}
}