{
	"system.install" : function(manifest,reply) {
		var save = this.queue("object.save");
		var fs   = this.require("fs");
		for (path in manifest){
			var href=manifest[path].href;
			return fs.readFile( href, function(err,source){
				save({
					path:path,
					value:{source:source}
				});
			});
		}
		return "Install[system.install]" + reply(null,true);
	}
}
