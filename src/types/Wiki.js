{
	"init" : function(regex,reply){
		var This=this;
		
		return "Wiki[init]" + This.bind("page.view",function(err,path){
			return This.queue("service.register",{
				regex : regex.read,
				path  : path
			},function(err,data){
				if (err) This.error(err);
				return This.bind("page.edit",function(err,path){
					return This.queue("service.register",{
						regex : regex.edit,
						path  : path
					},function(err,data){
						if (err) This.error(err);
						return reply(null,"OK");
					});
				});	
			});
		});
	},
	"page.view" : function(msg,reply){
		var path = msg.path;
		var dat = msg.data;
		return "Wiki[page.view]" + this.queue("object.read",path,function(err,object){
			if(err) return reply(err,null);
			return reply(null,{content:object,encoding:"application/json"});
		});
	},
	"page.edit" : function(msg,reply){
		var path = msg.path;
		var dat = msg.data;
		var item = {
			path  : path,
			value : dat
		}
		return "Wiki[page.edit]" + this.queue("object.save",item,function(err,object){
			if(err) return reply(err,null);
			return reply(null,{content:object,encoding:"application/json"});
		});
	}
}