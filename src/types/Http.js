{
	"service.register" : function(args,reply){
		var regex = args.regex;
		var path  = args.path;
		var mode  = args.mode;
		
		this.apps[regex] = {path:path,mode:mode};
		this.info("HTTP Service: %s → %s",regex,path);
		
		return "Http[service.register]" + reply(null,"OK");
	},
	"init" : function(port,reply) {
		var This  = this;
		
		var http  = this.require('http');
		var fmt   = this.require('sprintf').sprintf;
		var util  = this.require('util');
		
		var error = function(rep,message){
			
			rep.setHeader("Content-Type","application/json");
			rep.end(
				JSON.stringify({
					type:"error",
					message:message
				}),encoding="utf8"
			);
			
		}
		this.apps = {};
		
		if (this.server) reply(new Error("Server Already Started"),null);
		this.server=http.createServer( function(req,rep){
			
			for ( regex in This.apps ) {
				
				var re = new RegExp(regex);
				
				if ( !req.url.match( re ) ) {
					continue;
				} 
				
				var mode = This.apps[regex].mode;
				var path = This.apps[regex].path;
				
				var data = "";
				req.on('data',function(chunk){
					data += chunk;
				});
				req.on('end',function(){
					var msg = {};
					
					try {
						if (data) msg.data = JSON.parse(data);
					} catch ( Exception ){
						msg.data = {}
						This.error("JSON Message Failed :%s:",data);
					}
					
					msg.url  = req.url;
					msg.path = req.url.split( re )[1];
					
					This.queue(path,msg,function(err,reply){
						if (err) {
							rep.statusCode = 500;
							return error(rep,"Callback Error: " + util.inspect(err) );
						}
						
						var content  = reply.content;
						var mimetype = reply.mimetype;
						
						if (mimetype) rep.setHeader("Content-Type",mimetype);
						
						switch(mode){
							case "raw":
								rep.write( content );
								break;
							default:
								rep.write( JSON.stringify(content) );
						}
						
						return rep.end();
					});
				});
				return;
			}
			
			This.warning("HTTP Error - 404 %s",req.url);
			rep.statusCode=404;
			error(rep,"Application Not Found");
			
		});
		this.server.listen(port);
		This.info("Server Now Listening on Port %d",port);
		return "Http[init]" + reply(null,true);
	}
}

