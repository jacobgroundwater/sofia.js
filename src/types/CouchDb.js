{
	// Configure the database connectino
	"init": function(message,reply){
		var proc   = this;
		var cradle = this.require('cradle');
		
		proc.info(("Initializing Database"));
		
		if (!message) message={};
		var host = message.host ? message.host : "http://localhost";
		var port = message.port ? message.port : 5984;
		var name = message.name ? message.name : "sofiajs";
		var couch = new cradle.Connection( host, port, {
			cache: false,
			raw: false
		});
		
		proc.info("Cradle : Connecting to %s:%d/%s",host,port,name);
		
		var db = couch.database(name);
		
		this.db = db;
		
		return "CouchDb[init]" + db.exists(function(err,exists){
			if (err) return reply(err,null);
			if (exists) return reply(null,{host:host,port:port,name:name});
			proc.warning("   |-- Database [%s] Does Not Exist",name);
			return db.create(function(err,ok){
				if (err) return reply(err,null);
				proc.info("   |-- Database [%s] Created",name);
				return reply(null,{host:host,port:port,name:name});
			});
		});
	},
	"object.read" : function( message, reply ){
		return "CouchDb[object.read]" + this.db.get( message, reply );
	},
	"object.save" : function( message, reply ){
		var proc = this;
		
		if (!message)       return reply(this.Panic("Missing Message"),null);
		if (!message.value) return reply(this.Panic("Value Missing"),  null);
		if (!message.path && message.path!=="")  return reply(this.Panic("Path Missing"),   null);
		if (typeof message.value != 'object') return reply(this.Panic("Malformed Value"),null);
		
		var path  = message.path;
		var value = message.value;
		
		return "CouchDb[object.save]" + this.db.save( path, value, function(err,data){
			if (err) {
				proc.error("** Saved FAILED path: %s **",err);
				return reply(err,null);
			}
			return reply(null,data);
		});
	},
	"document.read" : function(message,reply){
		return "CouchDb[document.read]" + this.db.get(
			message.id,
			message.attachmentId,
			function(err,data){
				if (err) return reply(err,null);
				return reply(null,data);
			}
		);
	},
	"document.save" : function(message,reply){
		return "CouchDb[document.save]" + this.db.saveAttachment(
			message.id,
			message.rev,
			message.attachmentId,
			message.mimetype,
			message.body,
			function(err,data){
				if (err) return reply(err,null);
				return reply(null,data);
			}
		);
	}
}
