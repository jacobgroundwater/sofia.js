{
	"init" : function(ini,reply){
		var This=this;
		
		var regex = ini.regex;
		
		this.index      = ini.index ? ini.index : "index.html";
		this.directory  = ini.directory;
		
		this.bind("file.read",function(err,path){
			This.queue("service.register",{
				regex : regex,
				path  : path,
				mode  : "raw"
			},function(err,data){
				if (err) This.error(err);
			});
		});
		
		this.fileMappings = {
			".html" : "text/html",
			".css"  : "text/css",
			".json" : "application/json",
			".js"   : "text/javascript",
		}
		
		return "Static[init]" + reply(null,"OK");
		
	},
	"file.read" : function(args,reply){
		var This = this;
		
		var fs   = this.require("fs");
		var Path = this.require("path");
		
		var fullPath = Path.join( this.directory, args.path );
		
		var read = function(path,reply) {
			
			var ext = path.substring(path.lastIndexOf( "." ));

			var mimetype = This.fileMappings[ext] ? This.fileMappings[ext] : "text/plain";
			return fs.readFile( path, function(err,content){
				var data = {content:content,mimetype:mimetype};
				reply(err,data);
			});
		}
		
		return "Static[file.read]" + fs.stat( fullPath, function(err,stat){
			if (err || !stat.isFile() ) {
				var fullPathIndex = Path.join(fullPath,This.index);
				return fs.stat( fullPathIndex, function(err,stat){
					if (err) {
						This.error("Stat Error %j",err);
						return reply(err,null);
					}
					return read(fullPathIndex,reply);
				})
			} else {
				return read(fullPath,reply);
			}
		})
	}
}