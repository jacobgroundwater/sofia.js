{
	"init" : function(args,reply){
		var This=this;
		return "VersionStore[init]" + This.queue("branch.get","master",function(err,branchHash){
			if (!branchHash || err) {
				return This.queue("object.put",{},function(err,newBranchHash){
					if (err) {
						This.error("Object.Put Error %j",err);
						return reply(This.Panic("Init"),null);
					}
					return This.queue("branch.set",{
						name : "master",
						hash : newBranchHash
					},function(err,ok){
						if (err) {
							This.error("%j",err);
							return reply(This.Panic("Versions Init Error"),null);
						}
						return reply(null,true);
					});
				})
			} else {
				return reply(null,true);
			}
		})
	},
	"object.save" : function(keyValue,reply){
		var This=this;
		if (!keyValue) return reply(This.Panic("No Key-Value Specified"),null);
		
		var key   = keyValue.path;
		var value = keyValue.value;
		
		if (!key) return reply(This.Panic("No key specified"),null);
		
		return "VersionStore[object.save]" + This.queue("branch.get","master",function(err,branchHash){
			return This.queue("object.get",branchHash,function(err,branch){
				return This.queue("object.put",value,function(err,objectHash){
					branch[key] = objectHash;
					return This.queue("object.put",branch,function(err,newBranchHash){
						return This.queue("branch.set",{
							name:"master",hash:newBranchHash
						},reply);
					});
				});
			});
		});
	},
	"object.read" : function(key,reply){
		var This=this;
		return "VersionStore[object.read]" + This.queue("branch.get","master",function(err,branchHash){
			return This.queue("object.get",branchHash,function(err,branch){
				var objectHash = branch[key];
				return This.queue("object.get",objectHash,reply);
			})
		})
	}
}




