{
	"init" : function(name,reply){
		var This=this;
		var initName = name+"/init";
		return "Accounts[init]" + this.queue("init.fork",initName,function(err,pid){
			This.debug("Init PID: %s",pid);
		});
	}
}