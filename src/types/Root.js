{
	"init" : function(accounts,reply){
		var This=this;
		
		var accountKeys = Object.keys(accounts);
		
		f = function(){
			if (!accountKeys) return reply(null,true);
			accountKey = accountKeys.pop();
			if (!accountKey) return reply(null,true);
			
			return This.push("account.start",accountKey,function(err,pid){
				if (err) return "Root[init]:" + reply(
					This.Panic("Account %s Initialization Error %s",accountKey,err),null);
				f();
			})
		}
		
		return "Root[init]" + f();
		
	},
	"account.start" : function(account,reply){
		var This = this;
		var util = this.require("util");
		
		var forkName = "#INIT/" + account;
		var initPath = "/Account/" + account + "/init";
		
		// This.debug("Starting Account %s",account);
		
		// Fetch /init from the data store
		return "Root[account.start]" + This.queue("object.read",initPath,function(err,object){
			if (err) {This.error("%s : %j",initPath,err); return reply(1,null); }
			
			// Pass Through Existing Processes
			var pass = object.pass;
			var services = {};
			for (i in pass){
				var name = pass[i];
				This.queue("init.find",name,function(err,path){
					if (err) return reply(2,null);
					services[name] = path;
				})
			}
			
			init = {services:services};
			return This.queue("init.fork",{name:forkName,init:init},function(err,pid){
				if (err) return reply(3,null);
				
				// New Init Process
				return This.proc(pid,function(err,initProc){ 
					if (err) return reply(4,null);
					
					// Start New Processes
					var init = object.init;
					var f = function(){
						if (!init ) return reply(null,pid);
						var start = init.pop();
						if (!start) return reply(null,pid);
						return This.queue("object.read",start,function(err,item){
							if (err) return reply(This.Panic(),null);
							
							return initProc.push("process.start",item,function(err,pid){
								if (err) { 
									return reply(This.Panic("Proc Start Error: %j",err),null); 
								}
								
								This.info("Account %s Initialized",account);
								f();
							});
						})
					}
					f(); // Start chain
				});			
			})
		});
	}
}






