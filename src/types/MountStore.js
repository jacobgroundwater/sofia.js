{	
	"object.read" : function( message, reply ){
		var This = this;
		
		var reads = this.reads;
		
		var emit = new this.require("events").EventEmitter;
		emit.on('next',function(i){
			var read = reads[i];
			if (read){
				return This.queue(read,message,function(err,data){
					if (err) return emit.emit('next',i+1);
					reply(null,data);
				});
			}
		})
		return "MountStore[object.read]" + emit.emit(0);
	},
	"object.save" : function( message, reply ){
		var This = this;
		
		var saves = this.saves[i];
		
		var emit = new this.require("events").EventEmitter;
		emit.on('next',function(i){
			var save = saves[i];
			if (read){
				return This.queue(save,message,function(err,data){
					if (err) return emit.emit('next',i+1);
					reply(null,data);
				});
			}
		})
		return "MountStore[object.save]" + emit.emit(0);
	}
}