{
	"init" : function(regex,reply){
		var This = this;
		return "HttpAdmin[init]" + this.bind("handle",function(err,path){
			This.info("Network Port Bound to %s",path);
			return This.queue("service.register",{regex:regex,path:path},reply);
		});
	},
	"handle" : function(message,reply){
		return "HttpAdmin[handle]" + reply(null,"Message Received");
	}
}