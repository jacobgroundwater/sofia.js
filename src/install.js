#!/usr/bin/env node
var timers = require("timers");
var util   = require("util");
var cli    = require("cli-color");
var fs     = require("fs");
var events = require("events");
var vm     = require("vm");

var sofia = require("./system.js");

var argv  = process.argv;
var init     = argv[2],
	manifest = argv[3],
	verbose  = argv[4];

var manifestSource = JSON.parse( fs.readFileSync(manifest) );
var ini = {};

ini.db = JSON.parse( fs.readFileSync(init) );
ini.processes = {};
ini.emitter = new events.EventEmitter;
ini.network = {};
ini.stashes = {};

var kern = new sofia.Kernel( ini );

kern.parameters = {
	files : {host:'localhost',port:5984,name:'sofiajs'},
};


function bootSandbox(code,reply){

		var package;
		var proxy   = function(e){package=e;}
		var context = vm.createContext({proxy:proxy});
		
		try {
			vm.runInContext("proxy("+code+");",context);
			reply(null,package);
		} catch (Exception) {
			reply(Exception,null);
		}

}

var filesysProcess = fs.readFileSync("types/CouchDb.js");
var initialProcess = fs.readFileSync("types/Init.js");

bootSandbox(filesysProcess,function(err,package){
	if (err) throw err;
	kern.init(package,{},function(err,pid){
		if (err) throw err;
		kern.proc(pid,function(err,proc){
			if (err) throw err;
			proc.push("init",{},function(err,_){
				for (path in manifestSource){
					var href   = manifestSource[path].href;
					var value  = manifestSource[path].value;
					if (href) {
						var source = fs.readFileSync( href );
						bootSandbox(source,function(err,prototype){
							if (err) throw err;
							var value = {};
							for (name in prototype){
								value[name] = prototype[name].toString();
							}
							
							proc.push("object.save",{path:path,value:value},function(err,_){
								if (err) throw new Error("Installing Failed: "+path);
							});	
							
						})
					} else {
						proc.push("object.save",{path:path,value:value},function(err,_){
							if (err) throw err;
						})
					}
				};
			});
		});
	});
});
