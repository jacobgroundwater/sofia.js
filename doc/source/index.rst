.. SofiaJs documentation master file, created by
   sphinx-quickstart on Fri Sep 23 23:51:07 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=======
SofiaJs
=======

Ideas
=====

*   Simplify the `init.json` file
*   allow simpler `require()` usage without specific bindings


Contents:

.. toctree::
   :maxdepth: 2


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

